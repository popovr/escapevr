// Fill out your copyright notice in the Description page of Project Settings.

#include "MovableDoor.h"
#include "ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "Curves/CurveVector.h"
// Debug include
#include "Runtime/Engine/Classes/Engine/Engine.h"

// Sets default values
AMovableDoor::AMovableDoor()
{
 	PrimaryActorTick.bCanEverTick = true;

	// Create StaticMesh
	DoorFrameMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("DoorFrameMesh"));
	DoorFrameMeshComponent->SetupAttachment(RootComponent);
	DoorFrameMeshComponent->SetWorldScale3D(FVector(1.f, 1.f, 1.f));
	DoorFrameMeshComponent->Mobility = EComponentMobility::Type::Static;

	DoorMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("DoorMesh"));
	DoorMeshComponent->SetupAttachment(DoorFrameMeshComponent);
	DoorMeshComponent->SetRelativeLocation(FVector(200.f, 0.f, 110.f));
	DoorMeshComponent->SetWorldScale3D(FVector(.5f, .1f, 0.85f));

	// Assign Mesh
	ConstructorHelpers::FObjectFinder<UStaticMesh> DoorFrameAsset(TEXT("StaticMesh'/Game/Assets/Architecture/Wall_Door_400x300.Wall_Door_400x300'"));
	if (DoorFrameAsset.Succeeded())
	{
		DoorFrameMeshComponent->SetStaticMesh(DoorFrameAsset.Object);
	}
	ConstructorHelpers::FObjectFinder<UStaticMesh> CubeAsset(TEXT("StaticMesh'/Engine/EngineMeshes/Cube.Cube'"));
	if (CubeAsset.Succeeded())
	{
		DoorMeshComponent->SetStaticMesh(CubeAsset.Object);
	}
	// Assign Material
	ConstructorHelpers::FObjectFinder<UMaterialInterface> DoorFrameMaterialAsset(TEXT("Material'/Game/Assets/Materials/M_Basic_Wall.M_Basic_Wall'"));
	if (DoorFrameMaterialAsset.Succeeded())
	{
		DoorFrameMeshComponent->SetMaterial(0, DoorFrameMaterialAsset.Object);
	}
	ConstructorHelpers::FObjectFinder<UMaterialInterface> DoorMaterialAsset(TEXT("Material'/Game/Assets/Materials/M_AssetPlatform.M_AssetPlatform'"));
	if (DoorMaterialAsset.Succeeded())
	{
		DoorMeshComponent->SetMaterial(0, DoorMaterialAsset.Object);
	}

	// Setup Timeline
	ConstructorHelpers::FObjectFinder<UCurveVector> DoorOpeningCurveAsset(TEXT("CurveVector'/Game/Animations/Curve_DoorOpening.Curve_DoorOpening'"));
	if (DoorOpeningCurveAsset.Succeeded())
	{
		DoorOpeningCurve = DoorOpeningCurveAsset.Object;

		FOnTimelineVector TimelineCallback;
		FOnTimelineEventStatic TimelineFinishedCallback;

		TimelineCallback.BindUFunction(this, FName("DoorOpeningTick"));
		TimelineFinishedCallback.BindUFunction(this, FName("DoorOpeningCompleted"));

		DoorOpeningTimeline.AddInterpVector(DoorOpeningCurve, TimelineCallback);
		DoorOpeningTimeline.SetTimelineFinishedFunc(TimelineFinishedCallback);
	}
}

// Called when the game starts or when spawned
void AMovableDoor::BeginPlay()
{
	Super::BeginPlay();
	InitialLocation = DoorMeshComponent->GetRelativeTransform().GetLocation();
}

// Called every frame
void AMovableDoor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	DoorOpeningTimeline.TickTimeline(DeltaTime);
}

void AMovableDoor::DoorOpeningTick()
{
	float TimelineValue = DoorOpeningTimeline.GetPlaybackPosition();
	FVector OffsetLocation = DoorOpeningCurve->GetVectorValue(TimelineValue);
	//UE_LOG(LogTemp, Warning, TEXT("AMovableDoor: Tick [%f, %f, %f]"), OffsetLocation[0], OffsetLocation[1], OffsetLocation[2]);
	DoorMeshComponent->SetRelativeLocation(InitialLocation + OffsetLocation);
}

void AMovableDoor::ToggleDoor()
{
	if (IsOpen)
	{
		DoorOpeningTimeline.Reverse();
	}
	else
	{
		DoorOpeningTimeline.Play();
	}
	IsOpen = !IsOpen;
}

void AMovableDoor::DoorOpeningCompleted()
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, TEXT("AMovableDoor: Open"));
}
