// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/TimelineComponent.h"
#include "MovableDoor.generated.h"

UCLASS()
class ESCAPEVR_API AMovableDoor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMovableDoor();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Mesh)
	class UStaticMeshComponent* DoorMeshComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Mesh)
	class UStaticMeshComponent* DoorFrameMeshComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Animation)
	class UCurveVector *DoorOpeningCurve;

	UFUNCTION()
	void DoorOpeningTick();

	UFUNCTION(BlueprintCallable, Category = Actions)
	void ToggleDoor();

	UFUNCTION()
	void DoorOpeningCompleted();

	FTimeline DoorOpeningTimeline;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	FVector InitialLocation;
	bool IsOpen = false;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
