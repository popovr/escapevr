// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/TimelineComponent.h"
#include "PushButton2.generated.h"

UCLASS()
class ESCAPEVR_API APushButton2 : public AActor
{
	GENERATED_BODY()

public:
	APushButton2();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Mesh)
	class UStaticMeshComponent* BaseButtonMeshComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Mesh)
	class UStaticMeshComponent* ButtonMeshComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Mesh)
	class UBoxComponent* ButtonBoxCollisionComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Animation)
	class UCurveFloat* ButtonPushingCurve;

	UFUNCTION()
	void BeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	
	UFUNCTION()
	void ButtonPushingTick();

	UFUNCTION()
	void ButtonPushingCompleted();

	UFUNCTION(BlueprintNativeEvent, Category = Events)
	void ButtonPushEvent();
	void ButtonPushEvent_Implementation();

	FTimeline PushingTimeline;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	FVector InitialLocation;
	bool IsAnimating = false;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
