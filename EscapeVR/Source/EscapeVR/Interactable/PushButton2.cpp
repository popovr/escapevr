// Fill out your copyright notice in the Description page of Project Settings.

#include "PushButton2.h"
#include "ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "Curves/CurveFloat.h"
#include "Runtime/Engine/Classes/Engine/Engine.h"

// Sets default values
APushButton2::APushButton2()
{
	PrimaryActorTick.bCanEverTick = true;

	// Create StaticMesh
	BaseButtonMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BaseButtonMesh"));
	BaseButtonMeshComponent->SetupAttachment(RootComponent);
	BaseButtonMeshComponent->SetWorldScale3D(FVector(1.f, .5f, 1.f));
	BaseButtonMeshComponent->Mobility = EComponentMobility::Type::Static;

	ButtonMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ButtonMesh"));
	ButtonMeshComponent->SetupAttachment(BaseButtonMeshComponent);
	ButtonMeshComponent->SetRelativeLocation(FVector(0.f, 20.f, 0.f));
	ButtonMeshComponent->SetWorldScale3D(FVector(.8f, 1.f, 0.8f));

	ButtonBoxCollisionComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("ButtonBoxCollision"));
	ButtonBoxCollisionComponent->SetupAttachment(BaseButtonMeshComponent);
	ButtonBoxCollisionComponent->InitBoxExtent(FVector(25.f, 20.f, 25.f));
	ButtonBoxCollisionComponent->SetRelativeLocation(FVector(0.f, 30.f, 0.f));
	ButtonBoxCollisionComponent->SetGenerateOverlapEvents(true);
	ButtonBoxCollisionComponent->SetCollisionProfileName("Trigger");

	// Overlap Events
	ButtonBoxCollisionComponent->OnComponentBeginOverlap.AddDynamic(this, &APushButton2::BeginOverlap);

	// Assign Mesh
	ConstructorHelpers::FObjectFinder<UStaticMesh> CylinderAsset(TEXT("StaticMesh'/Engine/EngineMeshes/Cylinder.Cylinder'"));
	if (CylinderAsset.Succeeded())
	{
		BaseButtonMeshComponent->SetStaticMesh(CylinderAsset.Object);
		ButtonMeshComponent->SetStaticMesh(CylinderAsset.Object);
	}
	// Assign Material
	ConstructorHelpers::FObjectFinder<UMaterialInterface> BaseMaterialAsset(TEXT("Material'/Game/Assets/Materials/M_Wood_Walnut.M_Wood_Walnut'"));
	if (BaseMaterialAsset.Succeeded())
	{
		BaseButtonMeshComponent->SetMaterial(0, BaseMaterialAsset.Object);
	}
	ConstructorHelpers::FObjectFinder<UMaterialInterface> ButtonMaterialAsset(TEXT("Material'/Game/Assets/Materials/M_Wood_Oak.M_Wood_Oak'"));
	if (ButtonMaterialAsset.Succeeded())
	{
		ButtonMeshComponent->SetMaterial(0, ButtonMaterialAsset.Object);
	}

	// Setup Timeline
	ConstructorHelpers::FObjectFinder<UCurveFloat> PushingCurveAsset(TEXT("CurveFloat'/Game/Animations/Curve_ButtonPressing.Curve_ButtonPressing'"));
	if (PushingCurveAsset.Succeeded())
	{
		ButtonPushingCurve = PushingCurveAsset.Object;

		FOnTimelineFloat TimelineCallback;
		FOnTimelineEventStatic TimelineFinishedCallback;

		TimelineCallback.BindUFunction(this, FName("ButtonPushingTick"));
		TimelineFinishedCallback.BindUFunction(this, FName("ButtonPushingCompleted"));
		
		PushingTimeline.AddInterpFloat(ButtonPushingCurve, TimelineCallback);
		PushingTimeline.SetTimelineFinishedFunc(TimelineFinishedCallback);
	}
}

// Called when the game starts or when spawned
void APushButton2::BeginPlay()
{
	Super::BeginPlay();
	InitialLocation = ButtonMeshComponent->GetRelativeTransform().GetLocation();
}

// Called every frame
void APushButton2::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	PushingTimeline.TickTimeline(DeltaTime);
}

void APushButton2::BeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, TEXT("APushButton2: BeginOverlap"));
	if (OtherActor && !IsAnimating) {
		IsAnimating = true;
		PushingTimeline.PlayFromStart();
	}
}

void APushButton2::ButtonPushingTick()
{
	float TimelineValue = PushingTimeline.GetPlaybackPosition();
	float PressingDistance = ButtonPushingCurve->GetFloatValue(TimelineValue);
	//UE_LOG(LogTemp, Warning, TEXT("APushButton2: Tick %f"), PressingDistance);
	ButtonMeshComponent->SetRelativeLocation(FVector(InitialLocation[0], InitialLocation[1] - PressingDistance, InitialLocation[2]));
}

void APushButton2::ButtonPushingCompleted()
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, TEXT("APushButton2: Pressed"));
	IsAnimating = false;
	this->ButtonPushEvent();
}

void APushButton2::ButtonPushEvent_Implementation()
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, TEXT("APushButton2: ButtonPushEvent not implemented"));
}
