// Fill out your copyright notice in the Description page of Project Settings.

#include "PushButton1.h"
#include "ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "Runtime/Engine/Classes/Engine/Engine.h"

// Sets default values
APushButton1::APushButton1()
{
 	PrimaryActorTick.bCanEverTick = true;

	// Create StaticMesh
	BaseButtonMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BaseButtonMesh"));
	BaseButtonMeshComponent->SetupAttachment(RootComponent);
	BaseButtonMeshComponent->SetWorldScale3D(FVector(1.f, .5f, 1.f));
	BaseButtonMeshComponent->Mobility = EComponentMobility::Type::Static;

	ButtonMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ButtonMesh"));
	ButtonMeshComponent->SetupAttachment(BaseButtonMeshComponent);
	ButtonMeshComponent->SetRelativeLocation(FVector(0.f, 20.f, 0.f));
	ButtonMeshComponent->SetWorldScale3D(FVector(.8f, 1.f, 0.8f));

	ButtonBoxCollisionComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("ButtonBoxCollision"));
	ButtonBoxCollisionComponent->SetupAttachment(ButtonMeshComponent);
	ButtonBoxCollisionComponent->InitBoxExtent(FVector(30.f, 5.f, 30.f));
	ButtonBoxCollisionComponent->SetRelativeLocation(FVector(0.f, 10.f, 0.f));
	ButtonBoxCollisionComponent->SetGenerateOverlapEvents(true);
	ButtonBoxCollisionComponent->SetCollisionProfileName("Trigger");

	// Assign Mesh
	ConstructorHelpers::FObjectFinder<UStaticMesh> CylinderAsset(TEXT("StaticMesh'/Engine/EngineMeshes/Cylinder.Cylinder'"));
	if (CylinderAsset.Succeeded())
	{
		BaseButtonMeshComponent->SetStaticMesh(CylinderAsset.Object);
		ButtonMeshComponent->SetStaticMesh(CylinderAsset.Object);
	}
	// Assign Material
	ConstructorHelpers::FObjectFinder<UMaterialInterface> BaseMaterialAsset(TEXT("Material'/Game/Assets/Materials/M_Wood_Walnut.M_Wood_Walnut'"));
	if (BaseMaterialAsset.Succeeded())
	{
		BaseButtonMeshComponent->SetMaterial(0, BaseMaterialAsset.Object);
	}
	ConstructorHelpers::FObjectFinder<UMaterialInterface> ButtonMaterialAsset(TEXT("Material'/Game/Assets/Materials/M_Wood_Oak.M_Wood_Oak'"));
	if (ButtonMaterialAsset.Succeeded())
	{
		ButtonMeshComponent->SetMaterial(0, ButtonMaterialAsset.Object);
	}

	// Overlap Events
	ButtonBoxCollisionComponent->OnComponentBeginOverlap.AddDynamic(this, &APushButton1::BeginOverlap);
	ButtonBoxCollisionComponent->OnComponentEndOverlap.AddDynamic(this, &APushButton1::EndOverlap);
}

// Called when the game starts or when spawned
void APushButton1::BeginPlay()
{
	Super::BeginPlay();
	InitialLocation = ButtonMeshComponent->GetRelativeTransform().GetLocation();
}

// Called every frame
void APushButton1::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//UE_LOG(LogTemp, Warning, TEXT("APushButton1: DeltaTime %f"), DeltaTime);
	
	if (IsOverlapping) {
		PressDistance += DeltaTime * 100;
	} else {
		PressDistance -= DeltaTime * 100;
	}
	if (PressDistance > MaxPressDistance) {
		PressDistance = MaxPressDistance;
	}
	if (PressDistance < 0) {
		PressDistance = 0;
	}
	//UE_LOG(LogTemp, Warning, TEXT("APushButton1: Tick %f"), PressDistance);
	ButtonMeshComponent->SetRelativeLocation(FVector(InitialLocation[0], InitialLocation[1] - PressDistance, InitialLocation[2]));

	if (PressDistance == MaxPressDistance && !IsPressed) {
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, TEXT("APushButton1: Pressed"));
		IsPressed = true;
	}
}

void APushButton1::BeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, TEXT("APushButton1: BeginOverlap"));
	if (OtherActor) {
		IsOverlapping = true;
	}
}

void APushButton1::EndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, TEXT("APushButton1: EndOverlap"));
	IsOverlapping = false;
	IsPressed = false;
}