// Fill out your copyright notice in the Description page of Project Settings.

#include "TestActor.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystemComponent.h"
#include "Runtime/Engine/Classes/Engine/Engine.h"
#include "Curves/CurveFloat.h"

// Sets default values
ATestActor::ATestActor() : intensity(1.f), delay(0.f), extend(1.f), additionalIntensity(0.f)
{
	//PrimaryActorTick.bCanEverTick = true;
	// Create StaticMesh
	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMeshComponent->SetupAttachment(RootComponent);
	StaticMeshComponent->SetRelativeLocation(FVector(0.0f, 0.0f, 0.1f));
	StaticMeshComponent->SetWorldScale3D(FVector(.5f, .5f, 0.1f));
	ConstructorHelpers::FObjectFinder<UStaticMesh> CubeAsset(TEXT("StaticMesh'/Engine/EngineMeshes/Cube.Cube'"));

	if (CubeAsset.Succeeded())
	{
		StaticMeshComponent->SetStaticMesh(CubeAsset.Object);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("JumpPad: Failed to load Mesh"));
	}
	ConstructorHelpers::FObjectFinder<UMaterialInterface> MaterialAsset(TEXT("Material'/Game/Assets/Materials/M_Metal_Gold.M_Metal_Gold'"));
	if (MaterialAsset.Succeeded())
	{
		StaticMeshComponent->SetMaterial(0, MaterialAsset.Object);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("JumpPad: Failed to load Material"));
	}

	inter0 = Cast<ICollidable>(this);
	if (inter0)
	{
		inter0->Setup(this);
		this->OnActorBeginOverlap.AddDynamic(this, &ATestActor::BeginOverlap);
		inter = Cast<IInflamableActor>(this);
		if (inter) {
			inter->PutOnFire(this, this);
		}
	}

	
}

void ATestActor::Tick(float DeltaTime)
{
	//UE_LOG(LogTemp, Error, TEXT("ATestActor: Tick [%f] "), DeltaTime);
	Super::Tick(DeltaTime);
	inter->advanceTimeLine(DeltaTime);
}

void ATestActor::BeginOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("ATestActor: begin BIS3"));

	if (inter)
	{
		inter->setIntensity(intensity);
		inter->setAdditionalIntensity(additionalIntensity);
		inter->setDelay(delay);
		inter->extendTime(extend);
		inter->activateWithIntensity1(OverlappedActor, OtherActor);
	}
	
}

