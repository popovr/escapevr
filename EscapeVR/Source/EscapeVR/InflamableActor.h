// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "UObject/ObjectMacros.h"
#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Components/TimelineComponent.h"
#include "InflamableActor.generated.h"

// This class does not need to be modified.
UINTERFACE(Blueprintable)
class UInflamableActor : public UInterface
{
	GENERATED_BODY()
};


class AActor;

/**
 * 
 */
class ESCAPEVR_API IInflamableActor
{
	GENERATED_BODY()

public:
	UFUNCTION()
		virtual void PutOnFire(AActor* OverlappedActor, AActor* OtherActor);
	UFUNCTION()
		virtual void activate(AActor* OverlappedActor, AActor* OtherActor);
	UFUNCTION()
		virtual void activateWithIntensity1(AActor* OverlappedActor, AActor* OtherActor);

	UFUNCTION()
		virtual void FireIgnitionTick();

	UFUNCTION()
		virtual void FireIgnitionCompleted();

	UFUNCTION()
		virtual void advanceTimeLine(float DeltaTime);

	UFUNCTION()
		virtual void setIntensity(float iIntensity);
	UFUNCTION()
		virtual void setAdditionalIntensity(float iIntensity);
	UFUNCTION()
		virtual void setDelay(float delay = 0.f);
	UFUNCTION()
		virtual void extendTime(float factor);

	FTimeline FireIgnitionTimeline;
	class UParticleSystemComponent* FireParticleSystem;
	class UStaticMeshComponent* MainActorStaticMeshComponent;
	class UCurveFloat* FireIgnitionCurve;
	float intensity = 1.f;
	float initDelay;
	float fireDelay;
	float additionalIntensity = 0.f;
	FVector FireParticuleScaleVector;
};
