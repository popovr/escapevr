// Fill out your copyright notice in the Description page of Project Settings.

#include "Collidable.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "UObject/UObjectGlobals.h"

void ICollidable::Setup(AActor* actor)
{
	auto actorMainMesh = actor->GetComponentsByClass(UStaticMeshComponent::StaticClass())[0];
	check(actorMainMesh);
	MainActorStaticMeshComponent = Cast<UStaticMeshComponent>(actorMainMesh);
	FObjectInitializer ObjectInitializer(actor, actor, true, true);
	BoxCollision = ObjectInitializer.CreateDefaultSubobject<UBoxComponent>(actor, TEXT("BoxCollision"));
	BoxCollision->InitBoxExtent(FVector(100.f, 100.f, 100.f));
	BoxCollision->SetupAttachment(MainActorStaticMeshComponent);
	BoxCollision->SetRelativeLocation(FVector(0.0f, 0.0f, 100.f));
	BoxCollision->SetWorldScale3D(FVector(.5f, .5f, .1f));
	BoxCollision->SetGenerateOverlapEvents(true);
	BoxCollision->SetCollisionProfileName(TEXT("Trigger"));
}