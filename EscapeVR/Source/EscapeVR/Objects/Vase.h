// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Vase.generated.h"

UCLASS()
class ESCAPEVR_API AVase : public AActor
{
	GENERATED_BODY()
	
public:
	AVase();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Mesh)
	class UDestructibleComponent* VaseDestructibleMesh;

	UFUNCTION()
	void OnComponentHitEvent(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
	UFUNCTION()
	void OnComponentFractureEvent(const FVector & HitPoint, const FVector & HitDirection);

	UPROPERTY(EditAnywhere, Category = Sound)
	class USoundBase* OnDamageSound = nullptr;
	UPROPERTY(EditAnywhere, Category = Sound)
	class USoundBase* OnFractureSound = nullptr;
protected:
	virtual void BeginPlay() override;
	float LastHitTime = 0.f;
	bool IsBroken = false;

public:
	virtual void Tick(float DeltaTime) override;

};
