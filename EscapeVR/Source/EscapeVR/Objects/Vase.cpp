// Fill out your copyright notice in the Description page of Project Settings.

#include "Vase.h"
#include "DestructibleComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundBase.h"
#include "GameFramework/DamageType.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Runtime/Engine/Classes/Engine/Engine.h"

AVase::AVase()
{
	PrimaryActorTick.bCanEverTick = false;

	VaseDestructibleMesh = CreateDefaultSubobject<UDestructibleComponent>(TEXT("VaseDestructibleMesh"));
	VaseDestructibleMesh->SetupAttachment(RootComponent);
	
	// Physics setup
	//VaseDestructibleMesh->SetSimulatePhysics(true); // TODO: not working :(
	//VaseDestructibleMesh->PutAllRigidBodiesToSleep(); // TODO: not working :(

	// Collision setup
	VaseDestructibleMesh->SetGenerateOverlapEvents(false);
	VaseDestructibleMesh->SetNotifyRigidBodyCollision(true);
	VaseDestructibleMesh->SetCollisionProfileName("BlockAllDynamic");
	VaseDestructibleMesh->OnComponentHit.AddDynamic(this, &AVase::OnComponentHitEvent);
	VaseDestructibleMesh->OnComponentFracture.AddDynamic(this, &AVase::OnComponentFractureEvent);
}

void AVase::BeginPlay()
{
	Super::BeginPlay();
}

void AVase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AVase::OnComponentHitEvent(UPrimitiveComponent * HitComponent, AActor * OtherActor, UPrimitiveComponent * OtherComp, FVector NormalImpulse, const FHitResult & Hit)
{
	float CurrentHitTime = UKismetSystemLibrary::GetGameTimeInSeconds(GetWorld());
	if (OtherActor && OtherComp && OtherActor != this && (CurrentHitTime - LastHitTime > 0.05f) && !IsBroken)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("AVase: OnComponentHitEvent %f"), NormalImpulse.Size()));
		VaseDestructibleMesh->ApplyRadiusDamage(NormalImpulse.Size(), VaseDestructibleMesh->GetComponentLocation(), 0.f, NormalImpulse.Size() / 100.f, false);

		if (OnDamageSound) {
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), OnDamageSound, GetActorLocation());
		}
	}
	LastHitTime = CurrentHitTime;
}

void AVase::OnComponentFractureEvent(const FVector & HitPoint, const FVector & HitDirection)
{
	if (!IsBroken) {
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("AVase: OnComponentFractureEvent"));
		IsBroken = true;
		if (OnFractureSound) {
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), OnFractureSound, GetActorLocation());
		}
	}
}