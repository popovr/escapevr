// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "UObject/ObjectMacros.h"
#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Collidable.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UCollidable : public UInterface
{
	GENERATED_BODY()
};

class AActor;

/**
 * 
 */
class ESCAPEVR_API ICollidable
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION()
		virtual void Setup(AActor* actor);
	
	class UBoxComponent* BoxCollision;
	class UStaticMeshComponent* MainActorStaticMeshComponent;
};
