// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "BaseCharacter.generated.h"

UCLASS()
class ESCAPEVR_API ABaseCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ABaseCharacter();

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// First Person Camera
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	class USpringArmComponent* SpringArmComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	class UCameraComponent* FirstPersonCameraComponent;

	// Object Grabbing
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = ObjectGrabbing)
	class UPhysicsHandleComponent* PhysicsHandleComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = ObjectGrabbing)
	class USceneComponent* HeldObjectLocationComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = ObjectGrabbing)
	float GrabDistance;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = ObjectGrabbing)
	float ThrowForce;

	// Hand and fingers
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Mesh)
	class UStaticMeshComponent* Finger;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	// Called when the game starts or when spawned
	virtual void Tick(float DeltaSeconds) override;

	// Movement
	void MoveForward(float Val);
	void MoveRight(float Val);
	void Jump();

	// Object Grabbing
	void ThrowObject();
	void PickUpOrDropObject();
	void PickUpObject();
	void DropObject();
	
};
