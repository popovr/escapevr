// Fill out your copyright notice in the Description page of Project Settings.

#include "VRCharacter.h"
#include "ConstructorHelpers.h"
#include "Characters/VRHandComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/SceneComponent.h"
#include "Components/ArrowComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
// VR Include
#include "HeadMountedDisplay.h"
#include "HeadMountedDisplayFunctionLibrary.h"
// Debug Include
#include "Runtime/Engine/Classes/Engine/Engine.h"
//#include "DrawDebugHelpers.h"


AVRCharacter::AVRCharacter() :
	Super()
{
	PrimaryActorTick.bCanEverTick = true;

	// Capsule Component
	GetCapsuleComponent()->InitCapsuleSize(10.f, 0.f);
	GetCapsuleComponent()->SetGenerateOverlapEvents(true);

	// Debug only
	GetCapsuleComponent()->SetVisibility(true);
	GetCapsuleComponent()->SetHiddenInGame(false);
	GetArrowComponent()->SetVisibility(true);
	GetArrowComponent()->SetHiddenInGame(false);

	// Camera Offset Component
	VRCameraOffset = CreateDefaultSubobject<USceneComponent>(TEXT("VRCameraOffset"));
	VRCameraOffset->SetupAttachment(GetCapsuleComponent());

	// Camera Component
	VRCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("VRCamera"));
	VRCamera->SetupAttachment(VRCameraOffset);
	//VRCamera->bUsePawnControlRotation = true;
	VRCamera->bLockToHmd = true;

	// VR Motion Controllers
	R_VRHand = CreateDefaultSubobject<UVRHandComponent>(TEXT("R_VRHand"));
	R_VRHand->SetHandSide(UVRHandComponent::EVRHandSide::VE_Rigth);
	R_VRHand->SetupAttachment(VRCameraOffset);
	L_VRHand = CreateDefaultSubobject<UVRHandComponent>(TEXT("L_VRHand"));
	L_VRHand->SetHandSide(UVRHandComponent::EVRHandSide::VE_Left);
	L_VRHand->SetupAttachment(VRCameraOffset);

	// Charater Movement
	GetCharacterMovement()->AirControl = .8f;
}

void AVRCharacter::BeginPlay()
{
	Super::BeginPlay();
	UHeadMountedDisplayFunctionLibrary::SetTrackingOrigin(EHMDTrackingOrigin::Floor);
}

void AVRCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	HMDSyncMovements();
}

void AVRCharacter::HMDSyncMovements()
{
	FVector VRCameraLocation = VRCamera->GetRelativeTransform().GetLocation();
	FVector VRCameraOffsetLocation = VRCameraOffset->GetRelativeTransform().GetLocation();

	// Center camera with the capsule
	VRCameraOffset->SetRelativeLocation({ -VRCameraLocation[0], -VRCameraLocation[1], -GetCapsuleComponent()->GetScaledCapsuleRadius() });

	// Move actor in the space
	FVector MovementOffset = VRCameraLocation + VRCameraOffsetLocation;
	SetActorLocation(GetActorLocation() + FVector(MovementOffset[0], MovementOffset[1], 0.f), true);

	// Set capsule height
	//GetCapsuleComponent()->SetCapsuleHalfHeight(VRCameraLocation[2]);
}

void AVRCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	// Bind movement events
	PlayerInputComponent->BindAxis("MoveForward", this, &AVRCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AVRCharacter::MoveRight);

	// Bind action events
	PlayerInputComponent->BindAction("GrabRight", IE_Pressed, this, &AVRCharacter::GrabRight);
	PlayerInputComponent->BindAction("GrabRight", IE_Released, this, &AVRCharacter::ReleaseRight);
	PlayerInputComponent->BindAction("GrabLeft", IE_Pressed, this, &AVRCharacter::GrabLeft);
	PlayerInputComponent->BindAction("GrabLeft", IE_Released, this, &AVRCharacter::ReleaseLeft);
}

void AVRCharacter::MoveForward(float Value)
{
	if (Value)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("AVRCharacter: MoveForward"));
		AddMovementInput(VRCamera->GetForwardVector(), Value);
		//AddMovementInput(GetActorForwardVector(), Value);
	}
}

void AVRCharacter::MoveRight(float Value)
{
	if (Value)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("AVRCharacter: MoveRight"));
		AddMovementInput(VRCamera->GetRightVector(), Value);
		//AddMovementInput(GetActorRightVector(), Value);
	}
}

void AVRCharacter::GrabRight()
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("AVRCharacter: GrabRight"));
	R_VRHand->GrabHand();
}

void AVRCharacter::ReleaseRight()
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("AVRCharacter: ReleaseRight"));
	R_VRHand->ReleaseHand();
}

void AVRCharacter::GrabLeft()
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("AVRCharacter: GrabLeft"));
	//L_VRHand->GrabHand();
	L_VRHand->GrabToggle();
}

void AVRCharacter::ReleaseLeft()
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("AVRCharacter: ReleaseLeft"));
	//L_VRHand->ReleaseHand();
}
