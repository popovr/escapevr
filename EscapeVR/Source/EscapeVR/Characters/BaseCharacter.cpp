// Fill out your copyright notice in the Description page of Project Settings.

#include "BaseCharacter.h"
#include "ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/ArrowComponent.h"
#include "Components/InputComponent.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
// Debug Include
#include "Runtime/Engine/Classes/Engine/Engine.h"
#include "DrawDebugHelpers.h"


ABaseCharacter::ABaseCharacter() :
	Super(),
	GrabDistance(1000.f),
	ThrowForce(500.f)
{
	// Enable event Tick
	PrimaryActorTick.bCanEverTick = true;

	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(50.f, 100.f);
	GetCapsuleComponent()->SetGenerateOverlapEvents(true);

	// Debug only
	GetCapsuleComponent()->SetVisibility(true);
	GetCapsuleComponent()->SetHiddenInGame(false);
	GetArrowComponent()->SetVisibility(true);
	GetArrowComponent()->SetHiddenInGame(false);

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->RelativeLocation = FVector(0.f, 0.f, 50.f);
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a SpringArmComponent	
	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("FingerSpringArm"));
	SpringArmComponent->SetupAttachment(FirstPersonCameraComponent);
	SpringArmComponent->TargetArmLength = 0.f;

	// Create a HeldObjectLocation	
	HeldObjectLocationComponent = CreateDefaultSubobject<USceneComponent>(TEXT("HeldObjectLocation"));
	HeldObjectLocationComponent->SetupAttachment(SpringArmComponent);
	HeldObjectLocationComponent->RelativeLocation = FVector(200.f, 50.f, 0.f);

	// Create a PhysicsHandle
	PhysicsHandleComponent = CreateDefaultSubobject<UPhysicsHandleComponent>(TEXT("PhysicsHandle"));

	// Charater movement parameters
	GetCharacterMovement()->AirControl = 1.f;

	// Hand and fingers
	Finger = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Finger"));
	Finger->bCastDynamicShadow = false;
	Finger->CastShadow = false;
	Finger->SetupAttachment(SpringArmComponent);
	Finger->SetWorldLocation(FVector(100.0f, 30.0f, -30.f));
	Finger->SetWorldScale3D(FVector(.1f, .1f, .8f));
	Finger->SetRelativeRotation(FRotator(90.f, 0.f, 0.f));
	ConstructorHelpers::FObjectFinder<UStaticMesh> FingerAsset(TEXT("StaticMesh'/Engine/BasicShapes/Cylinder.Cylinder'"));
	if (FingerAsset.Succeeded()) Finger->SetStaticMesh(FingerAsset.Object);
	ConstructorHelpers::FObjectFinder<UMaterialInterface> MaterialAsset(TEXT("Material'/Game/Assets/Materials/M_Metal_Copper.M_Metal_Copper'"));
	if (MaterialAsset.Succeeded()) Finger->SetMaterial(0, MaterialAsset.Object);
}

void ABaseCharacter::BeginPlay()
{
	Super::BeginPlay();
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("BaseCharacter: BeginPlay"));
}

void ABaseCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	PhysicsHandleComponent->SetTargetLocation(HeldObjectLocationComponent->GetComponentLocation());
}

// Called to bind functionality to input
void ABaseCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// Bind movement events
	PlayerInputComponent->BindAxis("MoveForward", this, &ABaseCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ABaseCharacter::MoveRight);
	PlayerInputComponent->BindAxis("LookHorizontal", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookVertical", this, &APawn::AddControllerPitchInput);

	// Bind action events
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ABaseCharacter::Jump);
	PlayerInputComponent->BindAction("FirePrimary", IE_Pressed, this, &ABaseCharacter::ThrowObject);
	PlayerInputComponent->BindAction("FireSecondary", IE_Pressed, this, &ABaseCharacter::PickUpOrDropObject);
}

void ABaseCharacter::MoveForward(float Value)
{
	if (Value)
	{
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void ABaseCharacter::MoveRight(float Value)
{
	if (Value)
	{
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void ABaseCharacter::Jump()
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("BaseCharacter: Jump"));
	ACharacter::Jump();
}

void ABaseCharacter::ThrowObject()
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("BaseCharacter: ThrowObject"));
	if (PhysicsHandleComponent->GetGrabbedComponent()) {
		UPrimitiveComponent* GrabbedComponent = PhysicsHandleComponent->GetGrabbedComponent();
		DropObject();
		//GrabbedComponent->AddImpulse(FirstPersonCameraComponent->GetForwardVector()	* ThrowForce, NAME_None, true);
		GrabbedComponent->AddForce(FirstPersonCameraComponent->GetForwardVector()	* ThrowForce * 100.f, NAME_None, true);
	}
}

void ABaseCharacter::PickUpOrDropObject()
{
	if (PhysicsHandleComponent->GetGrabbedComponent()) {
		DropObject();
	}
	else
	{
		PickUpObject();
	}
}

void ABaseCharacter::PickUpObject()
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("BaseCharacter: PickUpObject"));
	FHitResult OutHit;
	FVector Start = FirstPersonCameraComponent->GetComponentLocation();
	FVector End = Start + (FirstPersonCameraComponent->GetForwardVector() * GrabDistance);

	DrawDebugLine(GetWorld(), Start, End, FColor::Green, false, 5, 0, 1);
	if (GetWorld()->LineTraceSingleByChannel(OutHit, Start, End, ECC_Visibility))
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("BaseCharacter: PickUpObject Hit"));
		UPrimitiveComponent* HitComponent = OutHit.Component.Get();
		if (OutHit.Component->IsSimulatingPhysics()) {
			//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("BaseCharacter: PickUpObject Hit SimulatingPhysics"));
			PhysicsHandleComponent->GrabComponentAtLocationWithRotation(HitComponent, NAME_None, HitComponent->GetComponentLocation(), HitComponent->GetComponentRotation());
		}
	}
}

void ABaseCharacter::DropObject()
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("BaseCharacter: DropObject"));
	PhysicsHandleComponent->ReleaseComponent();
}
