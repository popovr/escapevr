// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "VRCharacter.generated.h"

UCLASS()
class ESCAPEVR_API AVRCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	AVRCharacter();

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// Camera
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	class UCameraComponent* VRCamera;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	class USceneComponent* VRCameraOffset;

	// VR Hands
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = VRHand)
	class UVRHandComponent* R_VRHand;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = VRHand)
	class UVRHandComponent* L_VRHand;

protected:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;

	// Movement
	void HMDSyncMovements();
	void MoveForward(float Val);
	void MoveRight(float Val);
	void GrabRight();
	void ReleaseRight();
	void GrabLeft();
	void ReleaseLeft();
};
