// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "GameFramework/PlayerController.h"
#include "VRHandComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ESCAPEVR_API UVRHandComponent : public USceneComponent
{
	GENERATED_BODY()

public:
	enum class EVRHandSide : uint8
	{
		VE_None 	UMETA(DisplayName = "None"),
		VE_Rigth	UMETA(DisplayName = "Right"),
		VE_Left 	UMETA(DisplayName = "Left")
	};
	
	UVRHandComponent();

	// Motion Controllers
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Controller)
	class UMotionControllerComponent* MotionController;

	// Hands
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Mesh)
	class UStaticMeshComponent* HandMesh;

	// Object Grabbing
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = ObjectGrabbing)
	class UPhysicsHandleComponent* PhysicsHandle;
	
	UFUNCTION()
	void BeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void EndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

protected:
	virtual void BeginPlay() override;
	EVRHandSide HandSide = EVRHandSide::VE_None;
	class UPrimitiveComponent* OverlappedComponentToGrab = nullptr;
	FTransform OverlappedComponentToGrabOffset;

public:
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void SetHandSide(const EVRHandSide& HandSide);
	void GrabToggle();
	void GrabHand();
	void ReleaseHand();
	bool IsGrabbingObject();
	void PlayForceFeedback(EDynamicForceFeedbackAction::Type Action);
};
