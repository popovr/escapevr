// Fill out your copyright notice in the Description page of Project Settings.

#include "VRHandComponent.h"
#include "ConstructorHelpers.h"
#include "MotionControllerComponent.h"
#include "XRMotionControllerBase.h"
#include "HeadMountedDisplay.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Components/StaticMeshComponent.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "Kismet/GameplayStatics.h"
// Debug Include
#include "Runtime/Engine/Classes/Engine/Engine.h"

//#define DEBUG_PRINT(x) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT(x), true, { 2, 2 })
#define FORCE_FEEDBACK_DURATION  .3f
#define FORCE_FEEDBACK_INTENSITY .5f

UVRHandComponent::UVRHandComponent() : Super()
{
	PrimaryComponentTick.bCanEverTick = true;

	// Motion Controller
	MotionController = CreateDefaultSubobject<UMotionControllerComponent>(*GetName().Append(TEXT("_MotionController")));
	MotionController->SetupAttachment(this);

	HandMesh = CreateDefaultSubobject<UStaticMeshComponent>(*GetName().Append(TEXT("_HandMesh")));
	HandMesh->bCastDynamicShadow = false;
	HandMesh->CastShadow = false;
	HandMesh->SetupAttachment(MotionController);

	// Overlap events
	HandMesh->SetGenerateOverlapEvents(true);
	HandMesh->SetCollisionProfileName("OverlapAllDynamic");
	HandMesh->OnComponentBeginOverlap.AddDynamic(this, &UVRHandComponent::BeginOverlap);
	HandMesh->OnComponentEndOverlap.AddDynamic(this, &UVRHandComponent::EndOverlap);

	// Create a PhysicsHandle
	PhysicsHandle = CreateDefaultSubobject<UPhysicsHandleComponent>(*GetName().Append(TEXT("_PhysicsHandle")));
}

void UVRHandComponent::SetHandSide(const EVRHandSide& HandSide)
{
	this->HandSide = HandSide;
	TCHAR* HandMeshPath;

	switch (HandSide)
	{
	case EVRHandSide::VE_Rigth:
		MotionController->MotionSource = FXRMotionControllerBase::RightHandSourceId;
		HandMeshPath = TEXT("StaticMesh'/Engine/EngineMeshes/Sphere.Sphere'");
		break;
	case EVRHandSide::VE_Left:
		MotionController->MotionSource = FXRMotionControllerBase::LeftHandSourceId;
		HandMeshPath = TEXT("StaticMesh'/Engine/EngineMeshes/Sphere.Sphere'");
		break;
	default:
		// TODO throw ??
		UE_LOG(LogTemp, Error, TEXT("UVRHandComponent: HandSide not initialized"));
		return;
	}

	ConstructorHelpers::FObjectFinder<UStaticMesh> HandAsset(HandMeshPath);
	if (HandAsset.Succeeded())
	{
		HandMesh->SetStaticMesh(HandAsset.Object);
		HandMesh->SetRelativeScale3D(FVector(0.05f, 0.05f, 0.05f));
	}
}


void UVRHandComponent::BeginPlay()
{
	Super::BeginPlay();
	if (HandSide == EVRHandSide::VE_None)
	{
		// TODO Throw ??
		UE_LOG(LogTemp, Error, TEXT("UVRHandComponent: HandSide not initialized"));
	}
}

void UVRHandComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	PhysicsHandle->SetTargetLocationAndRotation(MotionController->GetComponentLocation(), MotionController->GetComponentRotation());
}

void UVRHandComponent::BeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	//DEBUG_PRINT("BeginOverlap");
	if (OtherActor && OtherComp && OtherActor != GetOwner() && !IsGrabbingObject())
	{
		OverlappedComponentToGrab = OtherComp;
		PlayForceFeedback(EDynamicForceFeedbackAction::Type::Start);
	}
}

void UVRHandComponent::EndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	//DEBUG_PRINT("UVRHandComponent: EndOverlap");
	if (!IsGrabbingObject()) {
		OverlappedComponentToGrab = nullptr;
		PlayForceFeedback(EDynamicForceFeedbackAction::Type::Stop);
	}
}

void UVRHandComponent::GrabToggle()
{
	if (!IsGrabbingObject() && OverlappedComponentToGrab){
		GrabHand();
	} else {
		ReleaseHand();
	}
}

void UVRHandComponent::GrabHand()
{
	//DEBUG_PRINT("UVRHandComponent: GrabHand");
	if (OverlappedComponentToGrab)
	{
		// Grabbing mode
		//DEBUG_PRINT("UVRHandComponent: GrabComponent");
		OverlappedComponentToGrabOffset = OverlappedComponentToGrab->GetRelativeTransform();
		PhysicsHandle->GrabComponentAtLocationWithRotation(OverlappedComponentToGrab, NAME_None, MotionController->GetComponentLocation(), MotionController->GetComponentRotation());
	}
	else
	{
		// Punching mode
		HandMesh->SetCollisionProfileName("BlockAllDynamic");
	}
}

void UVRHandComponent::ReleaseHand()
{
	//DEBUG_PRINT("UVRHandComponent: ReleaseHand");
	PhysicsHandle->ReleaseComponent();
	HandMesh->SetCollisionProfileName("OverlapAllDynamic");
}

bool UVRHandComponent::IsGrabbingObject()
{
	return !!PhysicsHandle->GetGrabbedComponent();
}

void UVRHandComponent::PlayForceFeedback(EDynamicForceFeedbackAction::Type Action)
{
	if (HandSide != EVRHandSide::VE_None) {
		bool RightHandSide = (HandSide == EVRHandSide::VE_Rigth);
		UGameplayStatics::GetPlayerController(GetWorld(), 0)->PlayDynamicForceFeedback(FORCE_FEEDBACK_INTENSITY, FORCE_FEEDBACK_DURATION, !RightHandSide, !RightHandSide, RightHandSide, RightHandSide, Action);
	}
}