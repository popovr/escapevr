// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "EscapeVRGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class ESCAPEVR_API AEscapeVRGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	AEscapeVRGameModeBase();

protected:
	virtual void BeginPlay() override;
};
