// Fill out your copyright notice in the Description page of Project Settings.

#include "DestructibleCapability.h"
#include "DestructibleComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundBase.h"
#include "Runtime/Engine/Classes/Engine/Engine.h"

UDestructibleCapability::UDestructibleCapability()
{
	PrimaryComponentTick.bCanEverTick = false;

	// Physics setup
	this->SetSimulatePhysics(true); // TODO: not working :(
	this->PutAllRigidBodiesToSleep(); // TODO: not working :(

	// Collision setup
	this->SetGenerateOverlapEvents(true);
	this->SetNotifyRigidBodyCollision(true);
	this->SetCollisionProfileName("BlockAllDynamic");
	this->OnComponentHit.AddDynamic(this, &UDestructibleCapability::OnComponentHitEvent);
	this->OnComponentFracture.AddDynamic(this, &UDestructibleCapability::OnComponentFractureEvent);
}

void UDestructibleCapability::BeginPlay()
{
	Super::BeginPlay();
}

void UDestructibleCapability::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UDestructibleCapability::OnComponentHitEvent(UPrimitiveComponent * HitComponent, AActor * OtherActor, UPrimitiveComponent * OtherComp, FVector NormalImpulse, const FHitResult & Hit)
{
	float CurrentHitTime = UKismetSystemLibrary::GetGameTimeInSeconds(GetWorld());
	if (OtherActor && OtherComp && (CurrentHitTime - LastHitTime > 0.05f) && !IsBroken)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("AVase: OnComponentHitEvent %f"), NormalImpulse.Size()));
		this->ApplyRadiusDamage(NormalImpulse.Size(), this->GetComponentLocation(), 0.f, NormalImpulse.Size() / 100.f, false);

		if (OnDamageSound) {
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), OnDamageSound, GetOwner()->GetActorLocation());
		}
	}
	LastHitTime = CurrentHitTime;
}

void UDestructibleCapability::OnComponentFractureEvent(const FVector & HitPoint, const FVector & HitDirection)
{
	if (!IsBroken) {
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("AVase: OnComponentFractureEvent"));
		IsBroken = true;
		if (OnFractureSound) {
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), OnFractureSound, GetOwner()->GetActorLocation());
		}
		OnFractureCustomEvent();
	}
}

void UDestructibleCapability::OnFractureCustomEvent_Implementation()
{
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, TEXT("UDestructibleCapability: OnFractureCustomEvent not implemented"));
}
