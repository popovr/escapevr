// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "DestructibleComponent.h"
#include "DestructibleCapability.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ESCAPEVR_API UDestructibleCapability : public UDestructibleComponent
{
	GENERATED_BODY()

public:
	UDestructibleCapability();

	UFUNCTION()
	void OnComponentHitEvent(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
	UFUNCTION()
	void OnComponentFractureEvent(const FVector & HitPoint, const FVector & HitDirection);

	UPROPERTY(EditAnywhere, Category = Sound)
	class USoundBase* OnDamageSound = nullptr;
	UPROPERTY(EditAnywhere, Category = Sound)
	class USoundBase* OnFractureSound = nullptr;

	UFUNCTION(BlueprintNativeEvent, Category = Events)
	void OnFractureCustomEvent();
	void OnFractureCustomEvent_Implementation();

protected:
	virtual void BeginPlay() override;
	float LastHitTime = 0.f;
	bool IsBroken = false;

public:
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

};
