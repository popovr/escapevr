// Fill out your copyright notice in the Description page of Project Settings.

#include "ShakingCapability.h"
#include "GameFramework/Actor.h"
#include "Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Engine/Engine.h"

UShakingCapability::UShakingCapability()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UShakingCapability::BeginPlay()
{
	Super::BeginPlay();
}

void UShakingCapability::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	FVector CurrentVelocityDirection;
	float CurrentVelocityLength;
	GetOwner()->GetVelocity().ToDirectionAndLength(CurrentVelocityDirection, CurrentVelocityLength);

	if (CurrentVelocityLength > ShakingVelocityThreashold) {
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("UShakingCapability: prev (%s), curr (%s), diff (%f), len (%f)"), *PreviousVelocityDirection.ToString(), *CurrentVelocityDirection.ToString(), DeltaVelocity.Size(), CurrentVelocityLength));
		FVector DeltaVelocity = CurrentVelocityDirection - PreviousVelocityDirection;

		if(DeltaVelocity.Size() > ShakingTollerance) {
			if (OnShakeSound) {
				UGameplayStatics::PlaySoundAtLocation(GetWorld(), OnShakeSound, GetOwner()->GetActorLocation());
			}
			OnShakeCustomEvent();
		}

		PreviousVelocityDirection = CurrentVelocityDirection;
	}
}

void UShakingCapability::OnShakeCustomEvent_Implementation()
{
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, TEXT("UShakingCapability: OnShakeCustomEvent not implemented"));
}
