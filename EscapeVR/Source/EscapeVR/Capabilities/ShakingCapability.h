// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ShakingCapability.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ESCAPEVR_API UShakingCapability : public UActorComponent
{
	GENERATED_BODY()

public:
	UShakingCapability();

	UPROPERTY(EditAnywhere, Category = Sound)
	class USoundBase* OnShakeSound = nullptr;

	UPROPERTY(EditAnywhere, Category = GameMechanics)
	float ShakingVelocityThreashold = 500.f;
	UPROPERTY(EditAnywhere, Category = GameMechanics, meta = (UIMin = "0.0", UIMax = "2.0", ToolTip = "Shaking Tollerance\n0: Each movement\n2: Only perfectly opposite movements\n1.4: 90deg movement"))
	float ShakingTollerance = 1.5f;

	UFUNCTION(BlueprintNativeEvent, Category = Events)
	void OnShakeCustomEvent();
	void OnShakeCustomEvent_Implementation();

protected:
	virtual void BeginPlay() override;
	FVector PreviousVelocityDirection;

public:
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

};
