// Fill out your copyright notice in the Description page of Project Settings.

#include "JumpPad.h"
#include "GameFramework/Character.h"
#include "ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "Runtime/Engine/Classes/Engine/Engine.h"

// Sets default values
AJumpPad::AJumpPad()
{
	// Create StaticMesh
	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMeshComponent->SetupAttachment(RootComponent);
	StaticMeshComponent->SetRelativeLocation(FVector(0.0f, 0.0f, 0.1f));
	StaticMeshComponent->SetWorldScale3D(FVector(.5f, .5f, 0.1f));
	ConstructorHelpers::FObjectFinder<UStaticMesh> CubeAsset(TEXT("StaticMesh'/Engine/EngineMeshes/Cube.Cube'"));
	if (CubeAsset.Succeeded())
	{
		StaticMeshComponent->SetStaticMesh(CubeAsset.Object);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("JumpPad: Failed to load Mesh"));
	}
	ConstructorHelpers::FObjectFinder<UMaterialInterface> MaterialAsset(TEXT("Material'/Game/Assets/Materials/M_Metal_Gold.M_Metal_Gold'"));
	if (MaterialAsset.Succeeded())
	{
		StaticMeshComponent->SetMaterial(0, MaterialAsset.Object);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("JumpPad: Failed to load Material"));
	}

	// Create Box Collition
	BoxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
	BoxCollision->InitBoxExtent(FVector(100.f, 100.f, 100.f));
	BoxCollision->SetupAttachment(StaticMeshComponent);
	BoxCollision->SetRelativeLocation(FVector(0.0f, 0.0f, 100.f));
	BoxCollision->SetWorldScale3D(FVector(.5f, .5f, .1f));
	BoxCollision->SetGenerateOverlapEvents(true);
	BoxCollision->SetCollisionProfileName(TEXT("Trigger"));

	this->OnActorBeginOverlap.AddDynamic(this, &AJumpPad::BeginOverlap);
}

void AJumpPad::BeginOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("JumpPad: BeginOverlap"));
	ACharacter* Player = Cast<ACharacter>(OtherActor);
	if (Player != nullptr) {
		Player->LaunchCharacter(FVector(0.f, 0.f, 1000.f), true, true);
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("JumpPad: It is not a BaseCharacter"));
		auto a = OtherActor->GetComponentsByClass(UStaticMeshComponent::StaticClass())[0];
		Cast<UPrimitiveComponent>(a)->SetPhysicsLinearVelocity(FVector(0.f, 0.f, 1300.f));
	}
}
