// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "InflamableActor.h"
#include "Collidable.h"
#include "TestActor.generated.h"

UCLASS()
class ESCAPEVR_API ATestActor : public AActor, public IInflamableActor, public ICollidable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATestActor();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Mesh)
		class UStaticMeshComponent* StaticMeshComponent;

	UFUNCTION()
		void BeginOverlap(AActor* OverlappedActor, AActor* OtherActor);

	IInflamableActor* inter;
	ICollidable* inter0;

	UPROPERTY(EditAnywhere, Category = Animation)
		float intensity;
	UPROPERTY(EditAnywhere, Category = Animation)
		float additionalIntensity;
	UPROPERTY(EditAnywhere, Category = Animation)
		float extend;
	UPROPERTY(EditAnywhere, Category = Animation)
		float delay;


public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
