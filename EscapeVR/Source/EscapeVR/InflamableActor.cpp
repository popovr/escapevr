// Fill out your copyright notice in the Description page of Project Settings.

#include "InflamableActor.h"
#include "ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystemComponent.h"
#include "Runtime/Engine/Classes/Engine/Engine.h"
#include "Components/StaticMeshComponent.h"
#include "UObject/UObjectGlobals.h"
#include "GameFramework/Character.h"
#include "Curves/CurveFloat.h"
#include "Runtime/Engine/Classes/Curves/RichCurve.h"



void IInflamableActor::PutOnFire(AActor* OverlappedActor, AActor* OtherActor)
{
	OverlappedActor->PrimaryActorTick.bCanEverTick = true;
	FObjectInitializer ObjectInitializer(OverlappedActor, OverlappedActor, true, true);
	FireParticleSystem = ObjectInitializer.CreateDefaultSubobject<UParticleSystemComponent>(OverlappedActor, TEXT("Fire Particle System"));

	ConstructorHelpers::FObjectFinder<UParticleSystem> FireAsset(TEXT("ParticleSystem'/Game/StarterContent/Particles/P_Fire.P_Fire'"));
	UParticleSystem* FireAssetObject = FireAsset.Object;
	
	auto actorMainMesh = OverlappedActor->GetComponentsByClass(UStaticMeshComponent::StaticClass())[0];
	MainActorStaticMeshComponent =  Cast<UStaticMeshComponent>(actorMainMesh);
	FireParticleSystem->SetupAttachment(MainActorStaticMeshComponent);
	FireParticleSystem->Template = FireAssetObject;
	FireParticleSystem->SetAutoActivate(false);
	FireParticleSystem->bAllowRecycling = true;
	FireParticleSystem->bCastDynamicShadow = true;
	UE_LOG(LogTemp, Warning, TEXT("PutOnFireFromInterface"));

	// Setup Timeline
	ConstructorHelpers::FObjectFinder<UCurveFloat> FireIgnitionCurveAsset(TEXT("CurveFloat'/Game/Animations/Curve_FireIgnition.Curve_FireIgnition'"));
	if (FireIgnitionCurveAsset.Succeeded())
	{
		FireIgnitionCurve = FireIgnitionCurveAsset.Object;

		FOnTimelineFloat TimelineCallback;
		FOnTimelineEventStatic TimelineFinishedCallback;

		TimelineCallback.BindUFunction(OverlappedActor, FName("FireIgnitionTick"));
		TimelineFinishedCallback.BindUFunction(OverlappedActor, FName("FireIgnitionCompleted"));

		FireIgnitionTimeline.AddInterpFloat(FireIgnitionCurve, TimelineCallback);
		FireIgnitionTimeline.SetTimelineFinishedFunc(TimelineFinishedCallback);
		FireIgnitionTimeline.SetPlaybackPosition(0.0f, false);
	}
	fireDelay = 0.f;
}

void IInflamableActor::FireIgnitionTick()
{
	float TimelineValue = FireIgnitionTimeline.GetPlaybackPosition();
	float intensityFloat = FireIgnitionCurve->GetFloatValue(TimelineValue) + additionalIntensity;
	/*UE_LOG(LogTemp, Warning, TEXT("relative scale [%f] multiply %f "), FireParticleSystem->RelativeScale3D.X, intensity*(FireParticleSystem->RelativeScale3D.X));
	UE_LOG(LogTemp, Warning, TEXT("relative scale [%f] multiply %f "), FireParticleSystem->RelativeScale3D.Y, intensity*(FireParticleSystem->RelativeScale3D.Y));
	UE_LOG(LogTemp, Warning, TEXT("relative scale [%f] multiply %f "), FireParticleSystem->RelativeScale3D.Z, intensity*(FireParticleSystem->RelativeScale3D.Z));*/
	
	FireParticleSystem->SetRelativeScale3D(intensity*intensityFloat*(FireParticuleScaleVector));
}

void IInflamableActor::FireIgnitionCompleted()
{
	FireParticleSystem->Deactivate();
	FireParticleSystem->SetRelativeScale3D(FireParticuleScaleVector);
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, TEXT("Fire Completed"));
}

void IInflamableActor::activate(AActor* OverlappedActor, AActor* OtherActor)
{
	ACharacter* Player = Cast<ACharacter>(OtherActor);
	
	if (FireParticleSystem)
	{
		if (Player)
		{
			FireParticleSystem->Activate();
		}
		else
		{
			FireParticleSystem->Deactivate();
		}

	}
	
}

void IInflamableActor::advanceTimeLine(float DeltaTime)
{
	FireIgnitionTimeline.TickTimeline(DeltaTime);
}

void IInflamableActor::activateWithIntensity1(AActor* OverlappedActor, AActor* OtherActor)
{
	FireParticleSystem->Activate();
	auto curves = FireIgnitionCurve->GetCurves();
	curves[0].CurveToEdit->DefaultValue = 1.f;
	auto timeFirstKey = curves[0].CurveToEdit->GetFirstKey().Time;
	if (!FireIgnitionTimeline.IsPlaying())
	{
		FireParticuleScaleVector = { FireParticleSystem->RelativeScale3D.X,  FireParticleSystem->RelativeScale3D.Y, FireParticleSystem->RelativeScale3D.Z };
		curves[0].CurveToEdit->ShiftCurve(fireDelay- timeFirstKey);
		FireIgnitionTimeline.PlayFromStart();
	}

}

void IInflamableActor::setIntensity(float iIntensity)
{
	intensity = iIntensity;
}

void IInflamableActor::setAdditionalIntensity(float iIntensity)
{
	additionalIntensity = iIntensity;
}

void IInflamableActor::setDelay(float delay)
{
	fireDelay = delay;
}

void IInflamableActor::extendTime(float factor)
{
	FireIgnitionTimeline.SetPlayRate(float(1.f/ factor));
}