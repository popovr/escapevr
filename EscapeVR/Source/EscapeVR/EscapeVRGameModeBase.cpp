// Fill out your copyright notice in the Description page of Project Settings.

#include "EscapeVRGameModeBase.h"
#include "UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Engine/Engine.h"
#include "Characters/BaseCharacter.h"
#include "Characters/VRCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"

AEscapeVRGameModeBase::AEscapeVRGameModeBase() : Super()
{
	// GEngine is false when UnrealEditor starts
	if (GEngine) {

		UE_LOG(LogTemp, Warning, TEXT("EscapeVRGameModeBase: IsHeadMountedDisplayConnected %d"), UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayConnected());
		UE_LOG(LogTemp, Warning, TEXT("EscapeVRGameModeBase: IsHeadMountedDisplayEnabled %d"), UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled());
		UE_LOG(LogTemp, Warning, TEXT("EscapeVRGameModeBase: EnableHMD %d"), UHeadMountedDisplayFunctionLibrary::EnableHMD(true));

		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayConnected() &&
			(
				UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled() ||
				UHeadMountedDisplayFunctionLibrary::EnableHMD(true)
			))
		{
			UE_LOG(LogTemp, Warning, TEXT("EscapeVRGameModeBase: VRMode"));
			DefaultPawnClass = AVRCharacter::StaticClass();
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("EscapeVRGameModeBase: DesktopMode"));
			DefaultPawnClass = ABaseCharacter::StaticClass();
		}
	}
}

void AEscapeVRGameModeBase::BeginPlay()
{
	Super::BeginPlay();
	UE_LOG(LogTemp, Warning, TEXT("EscapeVRGameModeBase: BeginPlay"));
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("EscapeVRGameModeBase: BeginPlay"));
}
